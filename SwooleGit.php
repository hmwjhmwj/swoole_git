<?php


//swoole工作流
class SwooleGit{

    private   $Server = null;
    private  $Address = null;
    private  $Config = [];

    public function __construct()
    {
        $this->Config = include "config.php";
    }



    public  function Run():void{
        $this->Server =  new  Swoole\Http\Server($this->Config['server']['host'],$this->Config['server']['port']);

        $this->Server->set(
            array('worker_num' => 2,
        ));
        $this->Server->on('WorkerStart', function () {

        });

        $this->Server->on('Request', function ($request, $response) {

            $data = [
                'get'=>$request->get,
                'post'=>$request->post,
            ];
            //此处不进行鉴权 需要自己处理
             if(false){
                echo "账户鉴权异常".date('Y-m-d H:i',time()). PHP_EOL;
                $response->end(json_encode(['code'=>0,'msg'=>'鉴权异常']));
                return;
            }else{

                 //获取webhook配置的 目录 cd到目录执行git pull
                array_unshift($this->Config['gitee']['cmds'],"cd ".$data['get']['list']);
                array_push($this->Config['gitee']['cmds'],"chown -R www:www ".$data['get']['list']);
                array_push($this->Config['gitee']['cmds'],"chmod -R 755 ".$data['get']['list']);
                echo "*****************************************************************************************************************************************".PHP_EOL;

                $cmd = implode(" && ",$this->Config['gitee']['cmds']);//拼接shell成一条一次性执行
                $info =  Swoole\Coroutine\System::exec($cmd);
                var_dump($info);//打印执行结果
                echo "===执行完成".date('Y-m-d H:i',time())."===". PHP_EOL;

            }

            $response->end(json_encode(['code'=>0,'msg'=>'任务已投递']));

        });

        $this->Server->start();
    }
}

$Server = new SwooleGit();
echo "开启服务";
$Server->Run();
