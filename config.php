<?php

return [
    'server'=>[
        'host'=>'0.0.0.0',
        'port'=>2349,
    ],

    'gitee'=>[
        'name'=>"1415181920",
        'cmds'=>[
            //   "ls",
            //   ,
            //   "cd	/www/wwwroot/****目录",
            //   "clear",
            //需要执行的shell命令自行添加
            "export COMPOSER_HOME=/root && composer install",
            "git reset --hard origin/master",
            "git pull" ,
        ],
    ]

];